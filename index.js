const express = require("express");
const server = express();
const PrCtrl = require("./controllers/project");

server.use(express.json());
server.listen(3000);

server.post("/projects", (req, res) => {
  const project = PrCtrl.create(req);
  return res.json(project);
});

server.get("/projects", (req, res) => {
  return res.json(PrCtrl.all());
});

server.put("/projects/:id", PrCtrl.exist, (req, res) => {
  const { id } = req.params;
  const project = PrCtrl.update(id, req);
  return res.json(project);
});

server.delete("/projects/:id", PrCtrl.exist, (req, res) => {
  const { id } = req.params;
  const project = PrCtrl.remove(id);
  return res.json(project);
});

server.post("/projects/:id/tasks", PrCtrl.exist, (req, res) => {
  const { id } = req.params;
  const { title } = req.body;
  const project = PrCtrl.addTask(id, title);
  return res.json(project);
});
