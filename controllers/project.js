const Projects = [];
const errors = {
  "project-not-found": {
    code: 0,
    message: "project does not exist"
  }
};
const all = () => {
  return Projects;
};
const create = req => {
  const { title, tasks } = req.body;
  const id = String(Projects.length);
  const project = { id, title, tasks };
  Projects.push(project);
  return project;
};

const find = id => {
  return Projects.find(pr => pr.id === id);
};

const update = (id, req) => {
  const { title } = req.body;
  const project = req.project ? req.project : projectFind(id);
  project.title = title;
  return project;
};

const remove = id => {
  let project;
  Projects.find((pr, index) => {
    if (pr.id === id) {
      Projects.splice(index, 1);
      project = pr;
    }
  });
  return project;
};

const addTask = (projectId, titleTask) => {
  const project = find(projectId);
  project.tasks.push(titleTask);
  return project;
};

const exist = (req, res, next) => {
  const { id } = req.params;
  const project = find(id);
  if (project) {
    req.project = project;
    return next();
  } else {
    return res.status(400).json({
      error: errors["project-not-found"]
    });
  }
};

module.exports = {
  all,
  create,
  find,
  update,
  remove,
  addTask,
  exist
};
